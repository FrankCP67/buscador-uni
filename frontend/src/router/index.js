import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
    },
    {
        path: '/alumnos',
        name: 'Alumnos',
        component: () => import(/* webpackChunkName: "home" */ '../views/alumno/MainAlumno.vue'),
        children: [
            {
                path: 'top',
                name: 'Mejores',
                component: () => import(/* webpackChunkName: "home" */ '../views/alumno/TopAlumno.vue')
            },
            {
                path: 'busqueda',
                name: 'Buscar por alumno',
                component: () => import(/* webpackChunkName: "home" */ '../views/alumno/SearchAlumno.vue'),
            },
        ],
    },

    {
        path: '/postulantes',
        name: 'Postulantes',
        component: () => import(/* webpackChunkName: "home" */ '../views/postulante/MainPostulante.vue'),
        children: [
            {
                path: 'results',
                name: 'Tabla postulantes',
                component: () => import(/* webpackChunkName: "home" */ '../views/postulante/DataTablePostulante.vue')
            },
            {
                path: 'busquedaAPI',
                name: 'Buscar por postulante API',
                component: () => import(/* webpackChunkName: "home" */ '../views/postulante/SearchPostulanteAPI.vue'),
            },
            {
                path: 'busqueda',
                name: 'Buscar por postulante',
                component: () => import(/* webpackChunkName: "home" */ '../views/postulante/SearchPostulante.vue'),
            },
        ]
    },
    {
        path: '/config',
        name: 'Configuración',
        component: () => import(/* webpackChunkName: "home" */ '../views/ConfigDialog.vue'),
        props: {activ: true}
    },
    {
        path: '*',
        name: 'Not Found',
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
