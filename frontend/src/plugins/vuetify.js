import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import es from 'vuetify/lib/locale/es'

import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                background: colors.red.accent4,
                card: colors.green.accent4,
                card2: colors.teal.accent3,
                iBlue: colors.blue.accent3,
            },
            dark: {
                background: colors.red.darken3,
                card: colors.green.darken4,
                card2: colors.teal.darken4,
                iBlue: colors.blue.darken2,
            },
        },
    },
    breakpoint: {
        mobileBreakpoint: 'sm' // This is equivalent to a value of 960
    },
    lang: {
        locales: {es},
        current: 'es',
    },
});
