import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookies from 'vue-cookies'
import wb from "./registerServiceWorker";
import VueRouterBackButton from 'vue-router-back-button'

Vue.config.productionTip = false

Vue.use(VueAxios, axios)
Vue.use(VueCookies)

Vue.prototype.$workbox = wb;
Vue.$cookies.config("30d", "", "", true);

Vue.use(VueRouterBackButton, {router, ignoreRoutesWithSameName: true})

new Vue({
    vuetify,
    router,
    render: h => h(App)
}).$mount('#app')
