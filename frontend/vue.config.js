// vue.config.js
module.exports = {
    // proxy all webpack dev-server requests starting with /api
    // to our Spring Boot backend (localhost:8098) using http-proxy-middleware
    // see https://cli.vuejs.org/config/#devserver-proxy
    devServer: {
        port: 8081,
        proxy: {
            '/api': {
                target: 'http://localhost:8080',
                ws: true,
                changeOrigin: true,
            }
        }
    },

    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = "Buscador UNI";
                return args;
            })
    },

    // Change build paths to make them Maven compatible
    // see https://cli.vuejs.org/config/
    outputDir: 'target/dist',

    assetsDir: 'static',

    transpileDependencies: [
        'vuetify'
    ],

    //PWA
    pwa: {
        name: "Buscador UNI",
        themeColor: "#B71C1C",
        msTileColor: "#f8e592",
        appleMobileWebAppCache: "yes",
        manifestOptions: {
            short_name: 'Buscador UNI',
            background_color: "#B71C1C",
            description: "Obtener datos de alumnos UNI",
            display: 'standalone',
            lang: 'es',
            orientation: 'natural',
            icons: [
                {
                    "src": "./img/icons/android-chrome-192x192.png",
                    "sizes": "192x192",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/android-chrome-512x512.png",
                    "sizes": "512x512",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/android-chrome-maskable-192x192.png",
                    "sizes": "192x192",
                    "type": "image/png",
                    "purpose": "maskable"
                },
                {
                    "src": "./img/icons/android-chrome-maskable-512x512.png",
                    "sizes": "512x512",
                    "type": "image/png",
                    "purpose": "maskable"
                },
                {
                    "src": "./img/icons/apple-touch-icon-60x60.png",
                    "sizes": "60x60",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-76x76.png",
                    "sizes": "76x76",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-120x120.png",
                    "sizes": "120x120",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-152x152.png",
                    "sizes": "152x152",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon-180x180.png",
                    "sizes": "180x180",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/apple-touch-icon.png",
                    "sizes": "180x180",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/favicon-16x16.png",
                    "sizes": "16x16",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/favicon-32x32.png",
                    "sizes": "32x32",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/msapplication-icon-144x144.png",
                    "sizes": "144x144",
                    "type": "image/png"
                },
                {
                    "src": "./img/icons/mstile-150x150.png",
                    "sizes": "150x150",
                    "type": "image/png"
                }
            ],
            shortcuts: [
                {
                    "name": "Datos de Alumnos",
                    "url": "/alumnos",
                    "icons": [
                        {
                            "src": "./img/icons/icon-alum.png",
                            "type": "image/png",
                            "sizes": "192x192"
                        }
                    ]
                },
                {
                    "name": "Datos de Postulantes",
                    "url": "/postulantes",
                    "icons": [
                        {
                            "src": "./img/icons/icon-post.png",
                            "type": "image/png",
                            "sizes": "192x192"
                        }
                    ]
                },
            ],
            scope:  '/'
        },
        workboxOptions: {
            navigateFallback: 'index.html'
        },
    }
}
