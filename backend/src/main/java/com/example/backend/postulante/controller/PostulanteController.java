package com.example.backend.postulante.controller;

import com.example.backend.postulante.dao.PostulanteDao;
import com.example.backend.postulante.model.Postulante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/postulantes")
public class PostulanteController {

    @Autowired
    PostulanteDao postulanteDao;

    @GetMapping("/ver")
    public List<Postulante> obtener(){
        return postulanteDao.findAll();
    }

    @GetMapping("/top/{nota}")
    public List<Postulante> obtenerTopAlumnos(@PathVariable("nota") Float nota){return postulanteDao.findByNotaVigesimalGreaterThanEqualOrderByAcumuladoDesc(nota);}

    @GetMapping("/verpostulante/{id}")
    public Postulante obtenerPostulante(@PathVariable("id") String id){
        return postulanteDao.findByNroInscripcion(id);
    }

    @PostMapping("/agregar")
    public void agregarPostulante(@RequestBody Postulante nuevo){
        postulanteDao.save(nuevo);
    }

    @DeleteMapping("/eliminar/{id}")
    public void eliminarPostulante(@PathVariable("id") String id){
        postulanteDao.deleteByNroInscripcion(id);
    }

}
