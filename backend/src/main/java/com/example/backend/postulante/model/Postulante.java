package com.example.backend.postulante.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class Postulante {
    @Id
    public String nroInscripcion;
    public String nombreCompleto;
    public Float prueba1;
    public Float prueba2;
    public Float prueba3;
    public Float acumulado;
    public Float notaVigesimal;

    public String getNroInscripcion() {
        return nroInscripcion;
    }

    public void setNroInscripcion(String nroInscripcion) {
        this.nroInscripcion = nroInscripcion;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Float getPrueba1() {
        return prueba1;
    }

    public void setPrueba1(Float prueba1) {
        this.prueba1 = prueba1;
    }

    public Float getPrueba2() {
        return prueba2;
    }

    public void setPrueba2(Float prueba2) {
        this.prueba2 = prueba2;
    }

    public Float getPrueba3() {
        return prueba3;
    }

    public void setPrueba3(Float prueba3) {
        this.prueba3 = prueba3;
    }

    public Float getAcumulado() {
        return acumulado;
    }

    public void setAcumulado(Float acumulado) {
        this.acumulado = acumulado;
    }

    public Float getNotaVigesimal() {
        return notaVigesimal;
    }

    public void setNotaVigesimal(Float notaVigesimal) {
        this.notaVigesimal = notaVigesimal;
    }
}