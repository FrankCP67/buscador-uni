package com.example.backend.postulante.dao;

import com.example.backend.postulante.model.Postulante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostulanteDao extends JpaRepository<Postulante,String> {
    Postulante findByNroInscripcion(String nroInscripcion);
    List<Postulante> findByNotaVigesimalGreaterThanEqualOrderByAcumuladoDesc(Float notaVigesimal);
    void deleteByNroInscripcion(String nroInscripcion);
}
