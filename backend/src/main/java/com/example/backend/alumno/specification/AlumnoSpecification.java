package com.example.backend.alumno.specification;

import com.example.backend.alumno.model.Alumno;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class AlumnoSpecification {
    public static Specification<Alumno> contains(String searchWord) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            Expression<String> nombreCompletoLowerCase = builder.lower(root.get("nombreCompleto"));
            Expression<String> codigoUNILowerCase = builder.lower(root.get("codigoUni"));
            Expression<String> facultadLowerCase = builder.lower(root.get("facultad"));
            Expression<String> especialidadLowerCase = builder.lower(root.get("especialidad"));
            predicates.add(builder.like(nombreCompletoLowerCase, "%" + searchWord.toLowerCase() + "%"));
            predicates.add(builder.like(codigoUNILowerCase, "%" + searchWord.toLowerCase() + "%"));
            predicates.add(builder.like(facultadLowerCase, "%" + searchWord.toLowerCase() + "%"));
            predicates.add(builder.like(especialidadLowerCase, "%" + searchWord.toLowerCase() + "%"));
            return builder.or(predicates.toArray(new Predicate[0]));
        };
    }
}
