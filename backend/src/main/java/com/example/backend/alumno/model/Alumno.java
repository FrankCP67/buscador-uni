package com.example.backend.alumno.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Alumno{
    @Id
    public String id;
    public String linkfoto;
    public String codigoUni;
    public String facultad;
    public String especialidad;
    public Integer cicloRelativo;
    public String nombreCompleto;
    public String rendimiento202;
    public Float nota212;
    public String idUniVirtual;

    public Float getNota212() {
        return nota212;
    }

    public void setNota212(Float nota212) {
        this.nota212 = nota212;
    }

    public String getCodigoUni() {
        return codigoUni;
    }

    public void setCodigoUni(String codigoUni) {
        this.codigoUni = codigoUni;
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getRendimiento202() {
        return rendimiento202;
    }

    public void setRendimiento202(String rendimiento202) {
        this.rendimiento202 = rendimiento202;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLinkfoto() {
        return linkfoto;
    }

    public void setLinkfoto(String linkfoto) {
        this.linkfoto = linkfoto;
    }

    public Integer getCicloRelativo() {
        return cicloRelativo;
    }

    public void setCicloRelativo(Integer cicloRelativo) {
        this.cicloRelativo = cicloRelativo;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getIdUniVirtual() {
        return idUniVirtual;
    }

    public void setIdUniVirtual(String idUniVirtual) {
        this.idUniVirtual = idUniVirtual;
    }
}
