package com.example.backend.alumno.dao;

import com.example.backend.alumno.model.Alumno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlumnoDao extends JpaRepository<Alumno,String>, JpaSpecificationExecutor<Alumno> {
    Alumno getByCodigoUni(String codigouni);
    void deleteByCodigoUni(String codigouni);
    List<Alumno> findByNota212IsNotNullOrderByNota212Desc();
}
