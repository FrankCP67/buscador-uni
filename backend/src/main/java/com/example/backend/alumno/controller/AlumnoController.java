package com.example.backend.alumno.controller;

import com.example.backend.alumno.dao.AlumnoDao;
import com.example.backend.alumno.model.Alumno;
import com.example.backend.alumno.specification.AlumnoSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/alumnos")
public class AlumnoController {

    @Autowired
    AlumnoDao alumnoDao;

    @GetMapping("/ver")
    public List<Alumno> obtener(){
        return alumnoDao.findAll();
    }

    @GetMapping("/top")
    public List<Alumno> obtenerTopAlumnos(){return alumnoDao.findByNota212IsNotNullOrderByNota212Desc();}

    @GetMapping("/veralumno/{id}")
    public Alumno obtenerAlumno(@PathVariable("id") String id){
        return alumnoDao.getByCodigoUni(id);
    }

    @PostMapping("/agregar")
    public void agregarAlumno(@RequestBody Alumno nuevo){
        alumnoDao.save(nuevo);
    }

    @DeleteMapping("/eliminar/{id}")
    public void eliminarAlumno(@PathVariable("id") String id){
        alumnoDao.deleteByCodigoUni(id);
    }

    @GetMapping("/buscar/{q}")
    public List<Alumno> buscarAlumnos(@PathVariable("q") String q) {
        List<String> words = Arrays.asList(q.split(" "));
        if(words.isEmpty()) {
            return Collections.emptyList();
        }
        Specification<Alumno> specification = null;
        for(String word : words) {
            Specification<Alumno> wordSpecification = AlumnoSpecification.contains(word);
            if(specification == null) {
                specification = wordSpecification;
            } else {
                specification = specification.and(wordSpecification);
            }
        }
        Page<Alumno> pageData = alumnoDao.findAll(specification,PageRequest.of(0, 100));
        return pageData.getContent();
    }
}
